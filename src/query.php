<?php 
	$GET_TABLE = 1;
	$UPDATE_ROW = 2;
	$DELETE_ROW = 3;
	$ADD_ROW = 4;

	$passwd = "1111"; 
	$db = new mysqli('localhost', 'levi', "$passwd", 'assignment4');

	if ($db->connect_error): 
	 die ("Could not connect to db " . $db->connect_error); 
	endif;
	
	if (isset($_POST["action"])) {
		// Return entire table
		if ($_POST["action"] == $GET_TABLE) {
			$q = "SELECT * FROM Recipes ORDER BY category ASC, rating ASC";
			$result = $db->query($q) or die("Error: " . $db->error);
			$data = "<categories>";
			$curr_cat = "";
			$first_cat = true;
			while ($row = $result->fetch_array()) {
				if ($row[2] != $curr_cat && !$first_cat) {
					$data .= "</$curr_cat>";
					$curr_cat = $row[2];
					$data .= "<$curr_cat>";
				}
				if ($first_cat) {
					$first_cat = false;
					$curr_cat = $row[2];
					$data .= "<$curr_cat>";
				}
				$data .= "<recipe>";
				$data .= "<id>$row[0]</id>";
				$data .= "<title>$row[1]</title>";
				$data .= "<ingredients>$row[3]</ingredients>";
				$data .= "<directions>$row[4]</directions>";
				$data .= "<rating>$row[5]</rating>";
				$data .= "</recipe>";
			}
			if ($curr_cat != "") $data .= "</$curr_cat>";
			$data .= "</categories>";
			$xml = simplexml_load_string($data);
			$json = json_encode($xml);
			echo $json;
		}
		else if ($_POST["action"] == $UPDATE_ROW) { // Update a row
			if (isset($_POST["id"]) && isset($_POST["field"]) && isset($_POST["value"])) {
				$id = $_POST["id"];
				$field = $_POST["field"];
				$value = $_POST["value"];
				
				// echo "id: $id, field: $field, value: $value";
				
				$q = "UPDATE Recipes SET $field='$value' WHERE id='$id'";
				$result = $db->query($q);
				if ($result) echo "Success";
				else echo $db->error;
			}
			else {
				echo "Proper post variables not supplied";
			}
		}
		else if ($_POST["action"] == $DELETE_ROW) { // Delete row
			if (isset($_POST["id"])) {
				$id = $_POST["id"];
				$q = "DELETE FROM Recipes WHERE id='$id'";
				$result = $db->query($q);
				if ($result) echo "Success";
				else echo $db->error;
			}
			else {
				echo "Proper post variables not supplied";
			}
		}
		else if ($_POST["action"] == $ADD_ROW) { // Add row
			if (isset($_POST["title"]) && isset($_POST["directions"]) && isset($_POST["rating"]) && isset($_POST["ingredients"]) && isset($_POST["category"])) {
				$title = $_POST["title"];
				$directions = $_POST["directions"];
				$rating = $_POST["rating"];
				$ingredients = $_POST["ingredients"];
				$category = $_POST["category"];

				$q = "insert into Recipes values (NULL, '$title', '$category', '$ingredients', '$directions', '$rating')"; 
				$result = $db->query($q);
				if ($result) echo "Success";
				else echo $db->error;
			}
			else echo "Proper post variables not supplied";
		}
	}
?>