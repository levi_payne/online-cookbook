<?php
	$passwd = "1111"; 
	$db = new mysqli('localhost', 'levi', "$passwd", 'assignment4');

	if ($db->connect_error): 
	 die ("Could not connect to db " . $db->connect_error); 
	endif;

	$db->query("drop table Recipes"); 

	$result = $db->query("create table Recipes (id int primary key not null auto_increment, title varchar(35) not null, category varchar(30), ingredients varchar(30), directions varchar(30), rating int(6))") 
		or die ("Invalid: " . $db->error);

	$xml=simplexml_load_file("cookbook.xml") or die("Error: Cannot create object");
	$json = json_encode($xml);
	$array = json_decode($json,TRUE);
	$cats = $array["categories"];
	// var_dump($cats);
	foreach ($cats as $cat => $stuff) {
		foreach ($stuff as $recipes) {
			foreach ($recipes as $recipe) {
				$title = $recipe["title"];
				$ingredients = $recipe["ingredients"];
				$directions = $recipe["directions"];
				$rating = $recipe["rating"];
				$query = "insert into Recipes values (NULL, '$title', '$cat', '$ingredients', '$directions', '$rating')"; 
				$db->query($query) or die ("Invalid insert " . $db->error);
			}
		}
	}
		
	echo "Successfully initialized!";
?>
